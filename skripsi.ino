// References
// https://theiotprojects.com/connect-rfid-to-php-mysql-database-with-nodemcu-esp8266/
// https://www.electronicshub.org/write-data-to-rfid-card-using-rc522-rfid/
// https://randomnerdtutorials.com/esp8266-nodemcu-https-requests/

// Dependencies
// MFRC522 by github community v.1.3.6

#include <SPI.h>
#include <MFRC522.h>

#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <WiFiClient.h>
#include <WiFiClientSecureBearSSL.h>

// Wifi Aauth
const char* ssid = "mf";
const char* password = "@pasipasi123";

//Your Domain name with URL path or IP address with path
String serverName = "https://skripsib.kuadran.co";

#define SS_PIN D2
#define RST_PIN D1
MFRC522 mfrc522(SS_PIN, RST_PIN);
#define ON_Board_LED 2

/* Create an instance of MIFARE_Key */
MFRC522::MIFARE_Key key;

/* Set the block to which we want to write data */
/* Be aware of Sector Trailer Blocks */
int blockNum = 2;
/* Create an array of 16 Bytes and fill it with data */
/* This is the actual data which is going to be written into the card */
byte blockData[] = { "Yng5aWo0bC8rV0xnT1NoSkRmNWFYYmZwVTNPZ1lEdnFzT0lkem8wPSBaVUM1d3o4OW4vT1prUy9pIHlXMW12dTd2WW5tU1BOeEpBenpLQWc9PQ==" };

/* Create another array to read data from Block */
/* Legthn of buffer should be 2 Bytes more than the size of Block (16 Bytes) */
byte bufferLen = 114;
byte readBlockData[114];

MFRC522::StatusCode status;

void setup() {
  Serial.begin(9600);

  setupWifi();
  setupRFID();
}

void setupWifi() {
  WiFi.begin(ssid, password);
  Serial.println("Connecting");
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to WiFi network with IP Address: ");
  Serial.println(WiFi.localIP());

  Serial.println("Timer set to 5 seconds (timerDelay variable), it will take 5 seconds before publishing the first reading.");
}

void setupRFID() {
  Serial.println("Start");
  SPI.begin();  // Init SPI bus
  mfrc522.PCD_Init();
  mfrc522.PCD_DumpVersionToSerial();
}

void loop() {
  byte a = byte(2);
  Serial.println(a);

  /* Prepare the ksy for authentication */
  /* All keys are set to FFFFFFFFFFFFh at chip delivery from the factory */
  for (byte i = 0; i < 6; i++) {
    key.keyByte[i] = 0xFF;
  }

  /* Look for new cards */
  /* Reset the loop if no new card is present on RC522 Reader */
  if (!mfrc522.PICC_IsNewCardPresent()) {
    // Serial.println("no card");
    return;
  }

  /* Select one of the cards */
  if (!mfrc522.PICC_ReadCardSerial()) {
    Serial.println("card serial");
    return;
  }

  //Serial.print("Card UID:");
  for (byte i = 0; i < mfrc522.uid.size; i++) {
    Serial.print(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " ");
    Serial.print(mfrc522.uid.uidByte[i], HEX);
  }
  Serial.println("");

  /* Print type of card (for example, MIFARE 1K) */
  Serial.print(F("PICC type: "));
  MFRC522::PICC_Type piccType = mfrc522.PICC_GetType(mfrc522.uid.sak);
  Serial.println(mfrc522.PICC_GetTypeName(piccType));


  // /* Call 'WriteDataToBlock' function, which will write data to the block */
  Serial.print("\n");
  Serial.println("Writing to Data Block...");
  WriteDataToBlock(blockNum, blockData);

  /* Read data from the same block */
  Serial.print("\n");
  Serial.println("Reading from Data Block...");
  ReadDataFromBlock(blockNum, readBlockData);
  /* If you want to print the full memory dump, uncomment the next line */
  //mfrc522.PICC_DumpToSerial(&(mfrc522.uid));

  // // Send data to server
  // char buffer[2048];
  // // sprintf(buffer, "{\"serial_number\":\"%s\",\"public_key\":\"%s\",\"cipher_text\":\"%s\"}", "a", "b", "c");
  // sprintf(buffer, "{\"serial_number\":\"%s\",\"public_key\":\"MjUxIzMjMjA0OCNbNjU1IDk2IDE3NjEgMjcyIDQ0NyAxMzE0IDc5NyAxMDc2IDE5NDQgMjA0NyAxNDI4IDg5OCAxMzk4IDE2NTEgMTE3NiAxOTkzIDE3MzEgNTE1IDc0MSA1OTEgMTkzNiAxNTM1IDYyNyA3NTggMTIxNCAxMTkxIDExNjMgMTU5IDE0NjAgNzI0IDcwOCAxMDYwIDk0OSAxNDIzIDE1NTggMjE3IDEyMzAgMTUwOSAxOTA0IDMxMiA1NTcgMzQgOTE4IDE2MjAgMTU5MSAxNTU0IDkyMSAxMzM1IDE2MDUgMTc0MSAxMjc5IDg3NiA2NzMgMzM5IDE5MjAgNjQgMTY1MyAxMjUgMTA2MSAxMzA0IDEzNDYgNDc4IDE4MDQgOTY1IDg4MSAxMjY0IDk0MyAxNTMwIDEzNzcgNTM2IDUyMCA2MDMgMTgxMiAyMDQ2IDE3OTggMzMxIDYyOCAxOTg1IDQ0MyA3NSA2NjUgNzY3IDQwNCAyMDQzIDk5OSAxMTQ2IDE1MzggMTI0NyAxMjk1IDk5IDE1ODggMjY4IDEzNjggMTgwMCAxMDU3IDE0NzEgNjg2IDEyMTcgNzc0IDg1NyAxMTgwIDIwOCA1MjkgMTgxMCAxMDkwIDE3OTYgMTY2MyAxNDE0IDE1NzcgNTQzIDkwMSAxNDQxIDE2NzEgNzg0IDEyNDUgNzkgMTM5NiAxODkyIDIwMTMgNzYxIDUyMSA3MzYgMTk1NCAxNjI2IDg4IDE3NTMgMTYwMSAxODcyIDIzOSAxMjUwIDg4NSA2MzYgNjY0IDk4MyAxMzY0IDE0MTggMjAyMiAxNzYzIDY0MCAxMTc3IDY3IDg1MSAxNDEgMTI0NyAxODgwIDE1MSAxMDE5IDIzOCAxNDE0IDExOTEgMTA5MSAxMzY3IDkwMCAyNDQgNTU2IDIwMTIgMTI1IDEyMzEgMTI1NCAxNDMzIDIwMzAgNTA5IDE1NDQgMTE5MiA5NDkgNzcwIDEyMTQgMTI1MiAxNzY3IDEzNTQgODU3IDUzNSA4MzcgMTMzMyAxMDU1IDIwMDQgMTQwMSAxMDk5IDI0IDIwMjQgMTQ5MyAyMjkgMTg5IDIwNDAgMTcxNCAxMTEwIDU2NCAzNDkgMTkzNyAxOTA0IDE1MzUgMTU0IDkyMSAxMjQ4IDEzNTIgNTYzIDE1NTYgMTM4MiAyNzggMzE1IDk1NiAxMjMzIDY2NyA2MDMgNzA1IDE3NSAyMDEyIDE0NyAxOTY3IDE3NDYgMTAgMTkxOSAxOTU5IDE3ODcgNDg0IDExNjQgMTQwOCAxNjMyIDUyMSAxNzkxIDEwMjIgOTkzIDk2NiAxMjk3IDk0OCAzMjAgNjAxIDUwIDIwMiAxMjY4IDEyNjMgMTUwMiAxOTYxIDYwNyAxMzQ5IDE1NDUgMjYzIDE5NzYgMTA3NyAxMDk1IDc0OCAxMiAxODg1IDE5MyAxMzQ1IDE2MCAxMDEwIDE1NTQgNjA4IDE4MDkgMTY5N10=\",\"cipher_text\":\"qbc\"}", "12345");
  // String data = String(buffer);

  // Serial.println(data);  
  // httpPost("card", data);

  /* Print the data read from block */
  Serial.print("\n");
  Serial.print("Data in Block:");
  Serial.print(blockNum);
  Serial.print(" --> ");
  for (int j = 0; j < 112; j++) {
    Serial.write(readBlockData[j]);
  }

  // Print result data
  // Update LED State

  Serial.print("\n");
  Serial.print("Clearing readblock\n");
  memset(&readBlockData[0], 0, sizeof(readBlockData));

  mfrc522.PICC_HaltA();
  mfrc522.PCD_StopCrypto1();
  delay(1000);
  return;
}

void httpGet() {
  if ((WiFi.status() == WL_CONNECTED)) {
    std::unique_ptr<BearSSL::WiFiClientSecure> client(new BearSSL::WiFiClientSecure);

    // Ignore SSL certificate validation
    client->setInsecure();

    //create an HTTPClient instance
    HTTPClient https;

    //Initializing an HTTPS communication using the secure client
    Serial.print("[HTTPS] begin...\n");
    if (https.begin(*client, serverName)) {  // HTTPS
      Serial.print("[HTTPS] GET...\n");
      // start connection and send HTTP header
      int httpCode = https.GET();
      // httpCode will be negative on error
      if (httpCode > 0) {
        // HTTP header has been send and Server response header has been handled
        Serial.printf("[HTTPS] GET... code: %d\n", httpCode);
        // file found at server
        if (httpCode == HTTP_CODE_OK || httpCode == HTTP_CODE_MOVED_PERMANENTLY) {
          String payload = https.getString();
          Serial.println(payload);
        }
      } else {
        Serial.printf("[HTTPS] GET... failed, error: %s\n", https.errorToString(httpCode).c_str());
      }

      https.end();
    } else {
      Serial.printf("[HTTPS] Unable to connect\n");
    }
  }
}

void httpPost(String path, String json) {
  Serial.println(json);
  if ((WiFi.status() == WL_CONNECTED)) {
    std::unique_ptr<BearSSL::WiFiClientSecure> client(new BearSSL::WiFiClientSecure);

    // Ignore SSL certificate validation
    client->setInsecure();

    //create an HTTPClient instance
    HTTPClient https;

    //Initializing an HTTPS communication using the secure client
    Serial.print("[HTTPS] begin...\n");
    if (https.begin(*client, serverName + "/" + path + "/")) {  // HTTPS
      Serial.print("[HTTPS] POST...\n");
      // start connection and send HTTP header
      https.addHeader("Content-Type", "application/json");
      int httpCode = https.POST(json);
      // httpCode will be negative on error
      if (httpCode > 0) {
        // HTTP header has been send and Server response header has been handled
        Serial.printf("[HTTPS] POST ... code: %d\n", httpCode);
        // file found at server
        if (httpCode == HTTP_CODE_OK || httpCode == HTTP_CODE_MOVED_PERMANENTLY) {
          String payload = https.getString();
          Serial.println(payload);
        }
      } else {
        Serial.printf("[HTTPS] GET... failed, error: %s\n", https.errorToString(httpCode).c_str());
      }

      https.end();
    } else {
      Serial.printf("[HTTPS] Unable to connect\n");
    }
  }
}

void WriteDataToBlock(int blockNum, byte blockData[]) {
  /* Authenticating the desired data block for write access using Key A */
  status = mfrc522.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A, blockNum, &key, &(mfrc522.uid));
  if (status != MFRC522::STATUS_OK) {
    Serial.print("Authentication failed for Write: ");
    Serial.println(mfrc522.GetStatusCodeName(status));
    return;
  } else {
    Serial.println("Authentication success");
  }


  /* Write data to the block */
  status = mfrc522.MIFARE_Write(blockNum, blockData, 16);
  if (status != MFRC522::STATUS_OK) {
    Serial.print("Writing to Block failed: ");
    Serial.println(mfrc522.GetStatusCodeName(status));
    return;
  } else {
    Serial.println("Data was written into Block successfully");
  }
}

void ReadDataFromBlock(int blockNum, byte readBlockData[]) {
  /* Authenticating the desired data block for Read access using Key A */
  byte status = mfrc522.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A, blockNum, &key, &(mfrc522.uid));

  if (status != MFRC522::STATUS_OK) {
    Serial.print("Authentication failed for Read: ");
    Serial.println(mfrc522.GetStatusCodeName((MFRC522::StatusCode)status));
    return;
  } else {
    Serial.println("Authentication success");
  }

  /* Reading data from the Block */
  status = mfrc522.MIFARE_Read(blockNum, readBlockData, &bufferLen);
  if (status != MFRC522::STATUS_OK) {
    Serial.print("Reading failed: ");
    Serial.println(mfrc522.GetStatusCodeName((MFRC522::StatusCode)status));
    return;
  } else {
    Serial.println("Block was read successfully");
    return;
  }
}

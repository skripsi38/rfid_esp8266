#include <SPI.h>
#include <MFRC522.h>

#define SS_PIN D2
#define RST_PIN D1
MFRC522 mfrc522(SS_PIN, RST_PIN);
#define ON_Board_LED 2

/* Create an instance of MIFARE_Key */
MFRC522::MIFARE_Key key;

/* Set the block to which we want to write data */
/* Be aware of Sector Trailer Blocks */
int blockNum = 2;
/* Create an array of 16 Bytes and fill it with data */
/* This is the actual data which is going to be written into the card */
byte blockData[] = { "Yng5aWo0bC8rV0xnT1NoSkRmNWFYYmZwVTNPZ1lEdnFzT0lkem8wPSBaVUM1d3o4OW4vT1prUy9pIHlXMW12dTd2WW5tU1BOeEpBenpLQWc9PQ==" };
byte blockDataPublicKey[] = { "29051f00e3412cf3627dbd406cdbca597abd769b4464db8ca3014688ea845be0" };

/* Create another array to read data from Block */
/* Legthn of buffer should be 2 Bytes more than the size of Block (16 Bytes) */
byte bufferLen = 114;
byte readBlockData[114];

MFRC522::StatusCode status;

void setup() {
  Serial.begin(38400);
  setupRFID();
}

void setupRFID() {
  Serial.println("Start");
  SPI.begin();  // Init SPI bus
  mfrc522.PCD_Init();
  mfrc522.PCD_DumpVersionToSerial();
}

void loop() {    
  /* Prepare the ksy for authentication */
  /* All keys are set to FFFFFFFFFFFFh at chip delivery from the factory */
  for (byte i = 0; i < 6; i++) {
    key.keyByte[i] = 0xFF;
  }

  /* Look for new cards */
  /* Reset the loop if no new card is present on RC522 Reader */
  if (!mfrc522.PICC_IsNewCardPresent()) {
    // Serial.println("no card");
    return;
  }

   /* Select one of the cards */
  if (!mfrc522.PICC_ReadCardSerial()) {
    Serial.println("card serial");
    return;
  }

  Serial.print("Card UID:");
  for (byte i = 0; i < mfrc522.uid.size; i++) {
    Serial.print(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " ");
    Serial.print(mfrc522.uid.uidByte[i], HEX);
  }
  Serial.println("");

  /* Print type of card (for example, MIFARE 1K) */
  Serial.print(F("PICC type: "));
  MFRC522::PICC_Type piccType = mfrc522.PICC_GetType(mfrc522.uid.sak);
  Serial.println(mfrc522.PICC_GetTypeName(piccType));

  /* Call 'WriteDataToBlock' function, which will write data to the block */
  Serial.print("\n");
  Serial.println("Writing to Data Block...");
  WriteDataToBlock(blockNum, blockData, blockDataPublicKey);

  /* Read data from the same block */
  Serial.print("\n");
  // Serial.println("Reading from Data Block...");
  // ReadDataFromBlock(blockNum, readBlockData);
  /* If you want to print the full memory dump, uncomment the next line */
  mfrc522.PICC_DumpToSerial(&(mfrc522.uid));

  mfrc522.PICC_HaltA();
  mfrc522.PCD_StopCrypto1();
  delay(1000);
  return;
}

void WriteDataToBlock(int blockNum, byte blockData[], byte blockDataPublicKey[]) {
  /* Write data to the block */
  int startIdx = 0;
  int stopIdx = 16;
  int block = 2;
  int n_cipher = ceil(strlen((char*)blockData)/16);
  int n_pub =  ceil(strlen((char*)blockDataPublicKey)/16);
  
  for (int i = 0; i < n_cipher; i++) {
    int size = 16;
    byte data[size];
    subData(blockData, data, startIdx, stopIdx);

    if (block == 3 || block == 7 || block == 11 || block == 15 || block == 19 || block == 23 || block == 27 || block == 31 || block == 35 || block == 39 || block == 43 || block == 47 || block == 51 || block == 55 || block == 59 || block == 63) {
      block++;
      Serial.println(block);
    }

    /* Authenticating the desired data block for write access using Key A */
    status = mfrc522.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A, block, &key, &(mfrc522.uid));
    if (status != MFRC522::STATUS_OK) {
      Serial.print("Authentication failed for Write: ");
      Serial.println(mfrc522.GetStatusCodeName(status));
      return;
    } else {
      Serial.println("Authentication success");
    }

    status = mfrc522.MIFARE_Write(block, data, 16);
    if (status != MFRC522::STATUS_OK) {
      Serial.print("Writing to Block failed: ");
      Serial.println(mfrc522.GetStatusCodeName(status));
      return;
    } else {
      Serial.println("Data was written into Block successfully");
    }

    startIdx += 16;
    stopIdx += 16;
    block++;
  }

  for (int i = 0; i < n_pub; i++) {
    int size = 16;
    byte data[size];
    subData(blockDataPublicKey, data, startIdx, stopIdx);

    if (block == 3 || block == 7 || block == 11 || block == 15 || block == 19 || block == 23 || block == 27 || block == 31 || block == 35 || block == 39 || block == 43 || block == 47 || block == 51 || block == 55 || block == 59 || block == 63) {
      block++;
      Serial.println(block);
    }

    /* Authenticating the desired data block for write access using Key A */
    status = mfrc522.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A, block, &key, &(mfrc522.uid));
    if (status != MFRC522::STATUS_OK) {
      Serial.print("Authentication failed for Write: ");
      Serial.println(mfrc522.GetStatusCodeName(status));
      return;
    } else {
      Serial.println("Authentication success");
    }

    status = mfrc522.MIFARE_Write(block, data, 16);
    if (status != MFRC522::STATUS_OK) {
      Serial.print("Writing to Block failed: ");
      Serial.println(mfrc522.GetStatusCodeName(status));
      return;
    } else {
      Serial.println("Data was written into Block successfully");
    }

    startIdx += 16;
    stopIdx += 16;
    block++;
  }
}

void subData(byte* blockData, byte* data, int startIndex, int stopIndex) {
  int n = stopIndex - startIndex;

  for (int i = startIndex; i < stopIndex; i++) {
    // cout <<  blockData[i] << endl;
    data[i - startIndex] = blockData[i];
    // cout << data[i] << endl;
  }
}
